import { Injectable, OnInit, INJECTOR } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { CoinModel, USD, Data } from '../models/coin.model';
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CoinService {


  coinssyms: string[] = ['BTC', 'LTC'];
  coins: CoinModel[] = [];

  constructor(private client: HttpClient) {
  }


  getSyms(): Observable<CoinModel[]> {
    return this.client.get<CoinModel>('https://api.coinmarketcap.com/v2/ticker/?limit=10').pipe(
      map((data) => {
        const _data = { ...data.data }
        return Object.keys(_data).map((key) => {
          console.log(key);
          return new CoinModel(new Data( +_data[key].id, _data[key].name, _data[key].symbol, _data[key].quotes))
        })
      }));
  }
  getcoinsym(id):Observable<CoinModel>{
    return this.client.get<CoinModel>('https://api.coinmarketcap.com/v2/ticker/' + id);
  }
}
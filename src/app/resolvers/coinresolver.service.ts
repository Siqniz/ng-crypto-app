import { Resolve,  RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router'
import { CoinService } from '../services/coin.service'
import { CoinModel } from '../models/coin.model';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class CoinResolverService implements Resolve<CoinModel>{

    constructor(private coinsvc: CoinService ) {}
    resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<CoinModel> | Promise<CoinModel> | CoinModel{
        return this.coinsvc.getcoinsym(route.params['id']);
    }
}
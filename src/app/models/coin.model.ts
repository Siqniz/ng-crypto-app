export class CoinModel {

    constructor(public data: Data) {

    }
}

export class Data {
    constructor(public id:number, public name: string, public symbol: string, public usd:USD) {
    }
}

export class USD {
    constructor(public price: number, public mktcap: number) {
    }
}
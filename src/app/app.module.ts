import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { RouterModule, Routes } from '@angular/router'

//service

//Mat

import { MatTableModule } from '@angular/material'

import { AppComponent } from './app.component';
import { CoinsComponent } from './coins/coins.component';
import { CoinComponent } from './Coins/coin/coin.component';
import { HighlightDirective } from './directives/highlight.directive';
import { CoinService } from './services/coin.service';
import { CoinResolverService } from './resolvers/coinresolver.service';

const appRoutes: Routes = [
  { path: '', component: CoinsComponent, children:[
    {path: ':id',  component:CoinComponent, resolve: {coin: CoinResolverService}}
  ]}
]

@NgModule({
  declarations: [
    AppComponent,
    CoinsComponent,
    CoinComponent,
    HighlightDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    MatTableModule
  ],
  providers: [CoinService, CoinResolverService],
  bootstrap: [AppComponent]
})
export class AppModule { }

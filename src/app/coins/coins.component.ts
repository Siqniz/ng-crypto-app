import { Component, OnInit } from '@angular/core';
import { CoinModel } from '../models/coin.model';
import { CoinService } from '../services/coin.service'

@Component({
  selector: 'app-coins',
  templateUrl: './coins.component.html',
  styleUrls: ['./coins.component.css']
})
export class CoinsComponent implements OnInit {

  dataSource;
  coins: CoinModel[] = [];
  displayedColumns: string[] = ['name', 'symbol', 'price', 'mktcap'];

  constructor(private coinsvc:CoinService) {
  }

  ngOnInit() {
    this.coinsvc.getSyms().subscribe(data => {
      this.coins = [...data];
      this.dataSource= this.coins;
    });
  }

}

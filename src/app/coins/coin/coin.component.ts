import { Component, OnInit } from '@angular/core';
import { Data, ActivatedRoute } from '@angular/router';
import { CoinResolverService } from '../../resolvers/coinresolver.service';

@Component({
  selector: 'app-coin',
  templateUrl: './coin.component.html',
  styleUrls: ['./coin.component.css']
})

export class CoinComponent implements OnInit {

  constructor(private route:ActivatedRoute ) { }
  currentCoin: string; 

  ngOnInit() {
    this.route.data.subscribe((data:Data) =>{
      this.currentCoin=data['coin'];
    });
 }
}


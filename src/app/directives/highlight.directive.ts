import { Directive, HostBinding, HostListener } from "@angular/core";


@Directive({
    selector: '[highlight]'
})
export class HighlightDirective {
    @HostBinding('style.backgroundColor') backgroundColor:string = 'lightGray'
    @HostBinding('style.cursor') cursor = "pointer";

    @HostListener('mouseenter') mouseover() {
        this.backgroundColor = 'orange'
    }

    @HostListener('mouseleave') mouseleave() {
        this.backgroundColor = 'lightGray';
    }
}